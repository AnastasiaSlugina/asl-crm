import React from "react";
import Head from "next/head";
import Link from "next/link";
import { unsetToken } from "../lib/auth";
import { Container, Nav, NavItem } from "reactstrap";
import defaultPage from "../hocs/defaultPage";
import { withRouter } from "next/router";
import { compose } from "recompose";

class Layout extends React.Component {
  constructor(props) {
    super(props);
  }
  static async getInitialProps({ req }) {
    let pageProps = {};
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }
  render() {
    const { children, isAuthenticated } = this.props;
    const title = "ASL CRM";
    return (
      <div>
        <Head>
          <title>{title}</title>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
          <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
            crossOrigin="anonymous"
          />
        </Head>
        <header>
          <Nav className="navbar navbar-dark bg-dark mb-4">
            <NavItem>
              <Link href="/">
                <a className="navbar-brand">Главная</a>
              </Link>
            </NavItem>
            {isAuthenticated ? (
              <>
                <NavItem>
                  <Link href="/taskList" as="/tasks">
                    <a className="navbar-brand">Задания</a>
                  </Link>
                </NavItem>
                <NavItem>
                  <Link href="/newTask" as="/tasks/add">
                    <a className="navbar-brand">Создать задание</a>
                  </Link>
                </NavItem>
                <NavItem className="ml-auto">
                  <span style={{ color: "white", marginRight: 30 }}>
                    {this.props.loggedUser}
                  </span>
                </NavItem>
                <NavItem>
                  <Link href="/">
                    <a className="logout" onClick={unsetToken}>
                      Выйти
                    </a>
                  </Link>
                </NavItem>
              </>
            ) : (
              <>
                <NavItem className="ml-auto">
                  <Link href="/signin">
                    <a className="nav-link">Авторизация</a>
                  </Link>
                </NavItem>

                {/* <NavItem>
                  <Link href="/signup">
                    <a className="nav-link"> Sign Up</a>
                  </Link>
                </NavItem> */}
              </>
            )}
          </Nav>
        </header>
        <Container>{children}</Container>
      </div>
    );
  }
}

export default compose(
  withRouter,
  defaultPage
)(Layout);