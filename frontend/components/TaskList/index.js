import gql from "graphql-tag";
import { graphql } from "react-apollo";
import {
  Alert
} from "reactstrap";

import TaskCard from "./TaskCard";

const TaskList = (
  { data: { loading, error, tasks }, search },
  req
) => {
  if (error) return (
    <Alert color="danger">Ошибка при получении заданий с сервера. Проверьте состояние и соединение с бекенд сервером</Alert>
  );
  //if tasks are returned from the GraphQL query, run the filter query
  //and set equal to variable tasksearch

  if (tasks && tasks.length) {
    //searchQuery
    const searchQuery = tasks.filter(query =>{
      if (query.name != null)
        return query.name.toLowerCase().includes(search)
      else 
        return query
    });
    if (searchQuery.length != 0) {
      return (
        <>
            {searchQuery.map(res => (
              <TaskCard task={res} key={res.id} showNav={true} leftCol="3" rightCol="9"/>
            ))}
        </>
      );
    } else {
      return <Alert fade={false} color="info">Задания не найдены</Alert>;
    }
  }
  return <Alert fade={false} color="info">Загружаю</Alert>;
};

const query = gql`
  {
    tasks {
      id
      created_at
      updated_at
      name
      target
      result
      dateBegin
      dateFinish
      priority
      tasksTypes
      responsible{
        id
        email
        FIO
        phoneNumbers
      }
      author{
        id
        email
        FIO
        phoneNumbers
      }
    }
  }
`;

// The `graphql` wrapper executes a GraphQL query and makes the results
// available on the `data` prop of the wrapped component (TaskList)
export default graphql(query, {
  props: ({ data }) => ({
    data
  })
})(TaskList);