export const EVENTTYPES = {
  'Public': 'Публичный',
  'Private': 'Личный',
  'Commercial': 'Коммерческий'
}

export const PRIORITIES = {
  'High': "Высокий",
  'Middle': "Средний",
  'Low': "Низкий"
}

