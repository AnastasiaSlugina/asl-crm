import React from 'react';

import { isEmpty } from 'lodash';
import Moment from 'moment';
import 'moment/locale/ru';
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardText,
  Row,
  Col
} from "reactstrap";
import ResponsibleList from './ResponsibleList';

export default class EventTask extends React.Component{
  constructor(){
    super();
  }

  render(){
    const { event, leftCol, rightCol } = this.props;
    const _leftCol = isEmpty(leftCol) ? "8" : leftCol;
    const _rightCol = isEmpty(rightCol) ? "4" : rightCol;
    const dateFormat = "DD.MM.YYYY в HH:mm:ss";
    if (event){
      const priorClasses = {
        'High': "danger",
        'Middle': "warning",
        'Low': "primary",
        '': ''
      };
      return(
        <Card className={`bg-${priorClasses[event.priority]}`}>
          <CardHeader><h5>{event.name}</h5></CardHeader>
          <CardBody>
            <Row>
              <Col sm={_leftCol}>
                <b>Участники</b><br />
                <ResponsibleList tag="p" members={event.members} />
              </Col>
              <Col sm={_rightCol}>
                Приоритет: {event.priority} <br />
                Тип: {event.types} <br />
                Ответственный: <ResponsibleList members={event.responsible} /> <br />
                Дата начала: {Moment(event.startAt).format(dateFormat)} <br />
                Сделать до: {Moment(event.dateFinish).format(dateFormat)} <br />
                Дата завершения: {Moment(event.endAt).format(dateFormat)} <br />
                Дата создания: {Moment(event.created_at).format(dateFormat)}
              </Col>
            </Row>
          </CardBody>
        </Card>
      )
    }
  }

}