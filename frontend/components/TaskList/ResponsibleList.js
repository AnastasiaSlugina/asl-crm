import React from 'react';
import { isPlainObject, isEmpty } from 'lodash'

/**
 * Выводит список props.members[] или одного props.members: {FIO, phoneNumbers, email}
 */
export default class ResponsibleList extends React.Component{
  render(){
    let { members, tag } = this.props;
    
    isPlainObject(members) ? members = [members] : null;

    const Rtag = ({tag, children, ...props}) => {
      tag = isEmpty(tag) ? "span" : tag;
      return React.createElement(tag, props, children)
    };
    
    return(
      <>
      {members.map( (resp, idx) => (
        <Rtag tag={tag} key={idx}>
          {resp.FIO}. Тел. "{resp.phoneNumbers}", email "{resp.email}"
        </Rtag>
      ))}
      </>
    )
  }
}