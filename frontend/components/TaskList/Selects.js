import React from 'react';
import { Input } from 'reactstrap';

export class ObjectSelect extends React.Component{
  
  render(){
    let {data} = this.props;
    data = (data || {});
    let opts = [];
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        opts.push(<option key={key} value={key}>{data[key]}</option>);
      }
    }
    
    return(
      <Input type="select" {...this.props}>
        <option disabled value="">Выберите значение</option>
        {opts.map( (opt) => 
          opt
        )}
      </Input>
    )
  }
}

export class ArraySelect extends React.Component{
  
  render(){
    let {data} = this.props;
    data = (data || []);
    // delete(this.props.data);
    return(
      <Input type="select" {...this.props}>
        <option disabled value="">Выберите значение</option>
        {data.map( (opt, idx) => 
          <option key={idx} value={opt.id} >{opt.name}</option>
        )}
      </Input>
    )
  }
}