import React from 'react';
import gql from "graphql-tag";
import { graphql } from "react-apollo";
import {
  Alert,
  Row,
  Col,
  Form,
  FormGroup,
  Label, Input, Button
} from "reactstrap";

import { ObjectSelect, ArraySelect} from './Selects';
import { EVENTTYPES, PRIORITIES } from './Enums';
import Moment from 'moment';
import 'moment/locale/ru';
import Cookies from 'js-cookie';

import getConfig from 'next/config';
const { publicRuntimeConfig } = getConfig();

import Strapi from 'strapi-sdk-javascript';
const strapi = new Strapi(publicRuntimeConfig.uploadURL);
import Router from "next/router";

class NewTaskForm extends React.Component{
  constructor(){
    super();
    this.state = {
      dateBegin: new Moment().format("YYYY-MM-DDTHH:mm"),
      dateFinish: new Moment().format("YYYY-MM-DDTHH:mm"),
      responsible: null,
      tasksTypes: Object.keys(EVENTTYPES)[0],
      priority: Object.keys(PRIORITIES)[0],
      author: Cookies.get("userId"),
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
  }

  onInputChange(e){
    const target = e.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.setState({[name]: value});
  }
  onSubmit(e){
    e.preventDefault();
    strapi.createEntry('tasks', this.state).then( (data) => {
      Router.push({ pathname: '/tasks/' + data.id });
    }).catch( error => this.setState({error}))
  }

  error(){
    if (this.state.error){
      return <Alert color="danger">{this.state.error}</Alert>
    }
  }

  render(){
    let { data: { loading, error, users } } = this.props;
    users = (users || []);

    const userList= users.map( (u) => ({
      id: u.id,
      name: u.FIO
    }))
    
    return(
      <Form onSubmit={this.onSubmit}>
        {this.error()}
        <h1>Новое задание</h1>
        <Row>
          <Col xs="6">
            <FormGroup>
              <Label>Название</Label>
              <Input onChange={this.onInputChange} type="text" name="name"/>
            </FormGroup>
            <FormGroup>
              <Label>Цель</Label>
              <Input onChange={this.onInputChange} type="text" name="target" />
            </FormGroup>
            <FormGroup>
              <Label>Результат</Label>
              <Input onChange={this.onInputChange} type="text" name="result" />
            </FormGroup>
            <FormGroup>
              <Label>Ожидаемый доход</Label>
              <Input onChange={this.onInputChange} type="text" name="expectedIncome" />
            </FormGroup>
            <FormGroup>
              <Label>Описание</Label>
              <Input onChange={this.onInputChange} type="textarea" name="content"/>
            </FormGroup>
            <FormGroup>
              <Label for="document">Файлы</Label>
              <div className="custom-file">
                <input type="file" className="custom-file-input" id="document" name="document" />
                <label className="custom-file-label" htmlFor="document">Выберите файл</label>
              </div>
            </FormGroup>
          </Col>
          <Col xs="6">
            <FormGroup>
              <Label>Ответственный</Label>
              <ArraySelect data={userList} name="responsible" defaultValue="" required onChange={this.onInputChange}/>
            </FormGroup>
            <FormGroup>
              <Label>Тип</Label>
              <ObjectSelect data={EVENTTYPES} name="tasksTypes" defaultValue={this.state.tasksTypes} required onChange={this.onInputChange}/>
            </FormGroup>
            <FormGroup>
              <Label>Приоритет</Label>
              <ObjectSelect data={PRIORITIES} name="priority" defaultValue={this.state.priority} required onChange={this.onInputChange}/>
            </FormGroup>
            <FormGroup>
              <Label>Дата начала</Label>
              <Input onChange={this.onInputChange} type="datetime-local" name="dateBegin" value={this.state.dateBegin} min={this.state.dateBegin} />
            </FormGroup>
            <FormGroup>
              <Label>Дата завершения</Label>
              <Input onChange={this.onInputChange} type="datetime-local" name="dateFinish" value={this.state.dateFinish} min={this.state.dateBegin}/>
            </FormGroup>
          </Col>
        </Row>
        <Button color="primary">Создать</Button>
      </Form>
    )
  }
}

const query = gql`
  {
    users(where:{ role_in: [4,5] } ){
      id
      username
      FIO
      phoneNumbers
      role{
        id
        name
      }
      organizations{
        id
        name
      }
    }
  }
`;

export default graphql(query, {
  props: ({ data }) => ({
    data
  })
})(NewTaskForm);