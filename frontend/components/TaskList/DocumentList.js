import React from 'react';
import { isPlainObject, isEmpty } from 'lodash';

import getConfig from 'next/config';
const { publicRuntimeConfig } = getConfig();

export default class DocumentList extends React.Component{
  render(){
    let { documents } = this.props;
    documents = (documents || []);
    return(
      <ul>
      {documents.map( (doc, idx) => (
        <li key={idx}><a href={publicRuntimeConfig.uploadURL + doc.url} target="_blank">{doc.name} ({doc.size}KB)</a></li>
      ))}
      </ul>
    )
  }
}