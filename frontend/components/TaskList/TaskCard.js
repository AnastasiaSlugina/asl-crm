import React from 'react';

import Moment from 'moment';
import 'moment/locale/ru';
import Link from "next/link";
import { isEmpty } from 'lodash';
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardText
} from "reactstrap";
import ResponsibleList from './ResponsibleList';

export default class TaskCard extends React.Component{
  constructor(){
    super();
  }

  render(){
    const { task, showNav, leftCol, rightCol } = this.props;
    const _leftCol = "col-sm-" + (isEmpty(leftCol) ? "6" : leftCol);
    const _rightCol = "col-sm-" + (isEmpty(rightCol) ? "6" : rightCol);
    return (
      <Card className="mb-2">
        <CardHeader><h5>{task.name}</h5><small className="text-muted">Создано {Moment(task.created_at).format("DD.mm.YYYY в HH:mm:ss")}</small></CardHeader>
        <CardBody>
          <CardText tag="div">
            <dl className="row">
              <dt className={_leftCol}>Цель</dt><dd className={_rightCol}>{task.target===null?"Нет":task.target}</dd>
              <dt className={_leftCol}>Результат</dt><dd className={_rightCol}>{task.result===null?"Нет":task.result}</dd>
              <dt className={_leftCol}>Ответственный</dt><dd className={_rightCol}><ResponsibleList members={task.responsible} /></dd>
              <dt className={_leftCol}>Автор</dt><dd className={_rightCol}><ResponsibleList members={task.author} /></dd>
            </dl>
          </CardText>
        </CardBody>
        {(!!showNav) ? (
          <CardFooter>
            <Link
              as={`/tasks/${task.id}`}
              href={`/task?id=${task.id}`}
            >
              <a className="btn btn-primary">Подробнее</a>
            </Link>
          </CardFooter>
        ) : ""}
      </Card>
    )
  }
}