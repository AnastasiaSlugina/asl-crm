import gql from "graphql-tag";
import { withRouter } from "next/router";
import { graphql } from "react-apollo";
import { compose } from "recompose";
import defaultPage from '../hocs/defaultPage';

import {
  Alert,
  Row,
  Col
} from "reactstrap";

import TaskCard from "../components/TaskList/TaskCard";
import EventTask from '../components/TaskList/EventTask';
import DocumentList from "../components/TaskList/DocumentList";

class TaskPage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      data: { loading, error, task },
      router,
      context,
      isAuthenticated
    } = this.props;
    if (error) return (
      <Alert color="danger">Ошибка при получении заданий с сервера. Проверьте состояние и соединение с бекенд сервером</Alert>
    );
    if (task) {
      return (
        <>
          <h1>Задание №{task.id}</h1>
          <Row>
            <Col sm="4">
              <TaskCard task={task} leftCol="12" rightCol="12"/>
            </Col>
            <Col sm="8">
              <h2>Описание</h2>
              {task.content || "Нет"}
              <h2>Документы</h2>
              <DocumentList documents={task.document} />
              <h2 className="mt-2">Мероприятия задания</h2>
              {task.events.map( (event) => (
                <EventTask event={event} key={event.id} leftCol="6" rightCol="6"/>
              ))}
            </Col>
          </Row>
        </>
      );
    }
    return <Alert fade={false} color="info">Загружаю</Alert>;
  }
}

const GET_TASK_EVENTS = gql`
  query($id: ID!) {
    task(id: $id) {
      id
      created_at
      updated_at
      name
      target
      result
      content
      dateBegin
      dateFinish
      priority
      tasksTypes
      document{
        id
        created_at
        name
        url
        size
      }
      responsible{
        id
        email
        FIO
        phoneNumbers
      }
      author{
        id
        email
        FIO
        phoneNumbers
      }
      events{
        id
        created_at
        startAt
        dateFinish
        endAt
        name
        types
        venue
        priority
        document{
          id
          created_at
          name
          url
          size
        }
        members{
          id
          email
          FIO
          phoneNumbers
        }
        responsible{
          id
          email
          FIO
          phoneNumbers
        }
        author{
          id
          email
          FIO
          phoneNumbers
        }
      }
    }
  }
`;
// The `graphql` wrapper executes a GraphQL query and makes the results
// available on the `data` prop of the wrapped component (Tasks)

export default compose(
  withRouter,
  defaultPage,
  graphql(GET_TASK_EVENTS, {
    options: props => {
      return {
        variables: {
          id: props.router.query.id
        }
      };
    },
    props: ({ data }) => ({ data })
  })
)(TaskPage);