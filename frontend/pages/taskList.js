import TaskList from "../components/TaskList";
import React from "react";
import Link from "next/link";
import defaultPage from '../hocs/defaultPage';
import { compose } from "recompose";

import {
  Button,
  Col,
  Input,
  InputGroup,
  InputGroupAddon,
  Row
} from "reactstrap";

class TaskListPage extends React.Component {
  constructor(props) {
    super(props);
    //query state will be passed to TaskList for the filter query
    this.state = {
      query: ""
    };
  }
  onChange(e) {
    //set the state = to the input typed in the search Input Component
    //this.state.query gets passed into TaskList to filter the results
    this.setState({ query: e.target.value.toLowerCase() });
  }
  render() {
    return (
      <div >
        <h1>Задания <Link href="/newTask" as="/tasks/add"><Button color="primary">+ Создать</Button></Link></h1>
          <div className="search mb-2 mt-2">
            <InputGroup>
              <InputGroupAddon addonType="append">Поиск задания</InputGroupAddon>
              <Input onChange={this.onChange.bind(this)} />
            </InputGroup>
          </div>
          <TaskList search={this.state.query} />
      </div>
    );
  }
}

export default compose(
  defaultPage
)(TaskListPage);
