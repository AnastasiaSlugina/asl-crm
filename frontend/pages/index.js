import React from "react";
import defaultPage from "../hocs/defaultPage";
import { compose } from "recompose";
import { withRouter } from "next/router";

class Index extends React.Component {
  constructor(props) {
    super(props);
    //query state will be passed to TaskList for the filter query
    this.state = {
      query: ""
    };
  }
  onChange(e) {
    //set the state = to the input typed in the search Input Component
    //this.state.query gets passed into TaskList to filter the results
    this.setState({ query: e.target.value.toLowerCase() });
  }
  render() {
    return (
      <div>
        Главная
      </div>
    );
  }
}

export default compose(
  withRouter,
  defaultPage
)(Index);