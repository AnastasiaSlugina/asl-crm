import React from 'react';
import { withRouter } from "next/router";
import { compose } from "recompose";
import defaultPage from '../hocs/defaultPage';

import NewTaskForm from '../components/TaskList/NewTaskForm';

class NewTask extends React.Component{
  render(){
    return (
      <NewTaskForm />
    );
  }
}

export default compose(
  withRouter,
  defaultPage
)(NewTask);