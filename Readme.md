# ASL CRM

Курсовая работа по дисциплине "Технология разработки программного обеспечения". 

Разработана с помощью:

1. https://strapi.io
2. https://nextjs.org

## Backend

```
cd ./backend 
```

### Install

```
npm i
```

### Start

```
strapi start
```

### Admin UI 

http://localhost:1337/admin

## Frontend

```
cd ./frontend
```

### Install

```
npm i
npm run-script build
```

### Start

```
npm start
```

### Frontend UI

http://localhost:3000