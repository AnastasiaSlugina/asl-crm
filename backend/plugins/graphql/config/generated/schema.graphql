type Contract {
  id: ID!
  created_at: DateTime!
  updated_at: DateTime!
  document: UploadFile
  contractStatus: ENUM_CONTRACT_CONTRACTSTATUS
  organization: Organization
  name: String
  createAt: DateTime
}

input ContractInput {
  document: ID
  contractStatus: ENUM_CONTRACT_CONTRACTSTATUS
  organization: ID
  name: String
  createAt: DateTime
}

input createContractInput {
  data: ContractInput
}

type createContractPayload {
  contract: Contract
}

input createEventsInput {
  data: EventsInput
}

type createEventsPayload {
  event: Events
}

input createOrganizationInput {
  data: OrganizationInput
}

type createOrganizationPayload {
  organization: Organization
}

input createReportInput {
  data: ReportInput
}

type createReportPayload {
  report: Report
}

input createRoleInput {
  data: RoleInput
}

type createRolePayload {
  role: UsersPermissionsRole
}

input createTasksInput {
  data: TasksInput
}

type createTasksPayload {
  task: Tasks
}

input createUserInput {
  data: UserInput
}

type createUserPayload {
  user: UsersPermissionsUser
}

"""
The `DateTime` scalar represents a date and time following the ISO 8601 standard
"""
scalar DateTime

input deleteContractInput {
  where: InputID
}

type deleteContractPayload {
  contract: Contract
}

input deleteEventsInput {
  where: InputID
}

type deleteEventsPayload {
  event: Events
}

input deleteOrganizationInput {
  where: InputID
}

type deleteOrganizationPayload {
  organization: Organization
}

input deleteReportInput {
  where: InputID
}

type deleteReportPayload {
  report: Report
}

input deleteRoleInput {
  where: InputID
}

type deleteRolePayload {
  role: UsersPermissionsRole
}

input deleteTasksInput {
  where: InputID
}

type deleteTasksPayload {
  task: Tasks
}

input deleteUserInput {
  where: InputID
}

type deleteUserPayload {
  user: UsersPermissionsUser
}

input editContractInput {
  document: ID
  contractStatus: ENUM_CONTRACT_CONTRACTSTATUS
  organization: ID
  name: String
  createAt: DateTime
}

input editEventsInput {
  name: String
  createAt: DateTime
  startAt: DateTime
  endAt: DateTime
  types: ENUM_EVENTS_TYPES
  members: [ID]
  venue: String
  priority: ENUM_EVENTS_PRIORITY
  dateFinish: DateTime
  document: [ID]
  tasks: ID
  author: ID
  responsible: ID
}

input editFileInput {
  name: String
  hash: String
  sha256: String
  ext: String
  mime: String
  size: String
  url: String
  provider: String
  public_id: String
  related: [ID]
}

input editOrganizationInput {
  name: String
  mail: String
  courierAddress: String
  contracts: [ID]
  reports: [ID]
  employees: [ID]
  phoneNumbers: String
}

input editReportInput {
  name: String
  dateCreate: DateTime
  editDate: DateTime
  departureDate: DateTime
  author: ID
  markDelete: Boolean
  organization: ID
}

input editRoleInput {
  name: String
  description: String
  type: String
  permissions: [ID]
  users: [ID]
}

input editTasksInput {
  responsible: ID
  tasksTypes: ENUM_TASKS_TASKSTYPES
  events: [ID]
  name: String
  target: String
  result: String
  dateBegin: DateTime
  dateCreate: DateTime
  dateFinish: DateTime
  priority: ENUM_TASKS_PRIORITY
  expectedIncome: String
  content: String
  document: [ID]
  author: ID
}

input editUserInput {
  username: String
  email: String
  provider: String
  password: String
  resetPasswordToken: String
  confirmed: Boolean
  blocked: Boolean
  role: ID
  reports: [ID]
  organizations: ID
  phoneNumbers: String
  FIO: String
  authorEvents: ID
  responsibleEvent: ID
  authorTask: ID
  taskResponsible: ID
  memberEvents: ID
}

enum ENUM_CONTRACT_CONTRACTSTATUS {
  Open
  Payed
  Closed
}

enum ENUM_EVENTS_PRIORITY {
  High
  Middle
  Low
}

enum ENUM_EVENTS_TYPES {
  Public
  Private
  Commercial
}

enum ENUM_TASKS_PRIORITY {
  High
  Middle
  Low
}

enum ENUM_TASKS_TASKSTYPES {
  Public
  Private
  Commercial
}

type Events {
  id: ID!
  created_at: DateTime!
  updated_at: DateTime!
  name: String
  createAt: DateTime
  startAt: DateTime
  endAt: DateTime
  types: ENUM_EVENTS_TYPES
  venue: String
  priority: ENUM_EVENTS_PRIORITY
  dateFinish: DateTime
  tasks: Tasks
  author: UsersPermissionsUser
  responsible: UsersPermissionsUser
  members(sort: String, limit: Int, start: Int, where: JSON): [UsersPermissionsUser]
  document(sort: String, limit: Int, start: Int, where: JSON): [UploadFile]
}

input EventsInput {
  name: String
  createAt: DateTime
  startAt: DateTime
  endAt: DateTime
  types: ENUM_EVENTS_TYPES
  members: [ID]
  venue: String
  priority: ENUM_EVENTS_PRIORITY
  dateFinish: DateTime
  document: [ID]
  tasks: ID
  author: ID
  responsible: ID
}

input FileInput {
  name: String!
  hash: String!
  sha256: String
  ext: String
  mime: String!
  size: String!
  url: String!
  provider: String!
  public_id: String
  related: [ID]
}

input InputID {
  id: ID!
}

"""
The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf).
"""
scalar JSON

union Morph = UsersPermissionsMe | UsersPermissionsMeRole | Contract | createContractPayload | updateContractPayload | deleteContractPayload | Events | createEventsPayload | updateEventsPayload | deleteEventsPayload | Organization | createOrganizationPayload | updateOrganizationPayload | deleteOrganizationPayload | Report | createReportPayload | updateReportPayload | deleteReportPayload | Tasks | createTasksPayload | updateTasksPayload | deleteTasksPayload | UploadFile | UsersPermissionsPermission | UsersPermissionsRole | createRolePayload | updateRolePayload | deleteRolePayload | UsersPermissionsUser | createUserPayload | updateUserPayload | deleteUserPayload

type Mutation {
  createContract(input: createContractInput): createContractPayload
  updateContract(input: updateContractInput): updateContractPayload
  deleteContract(input: deleteContractInput): deleteContractPayload
  createEvents(input: createEventsInput): createEventsPayload
  updateEvents(input: updateEventsInput): updateEventsPayload
  deleteEvents(input: deleteEventsInput): deleteEventsPayload
  createOrganization(input: createOrganizationInput): createOrganizationPayload
  updateOrganization(input: updateOrganizationInput): updateOrganizationPayload
  deleteOrganization(input: deleteOrganizationInput): deleteOrganizationPayload
  createReport(input: createReportInput): createReportPayload
  updateReport(input: updateReportInput): updateReportPayload
  deleteReport(input: deleteReportInput): deleteReportPayload
  createTasks(input: createTasksInput): createTasksPayload
  updateTasks(input: updateTasksInput): updateTasksPayload
  deleteTasks(input: deleteTasksInput): deleteTasksPayload

  """Create a new role"""
  createRole(input: createRoleInput): createRolePayload

  """Update an existing role"""
  updateRole(input: updateRoleInput): updateRolePayload

  """Delete an existing role"""
  deleteRole(input: deleteRoleInput): deleteRolePayload

  """Create a new user"""
  createUser(input: createUserInput): createUserPayload

  """Update an existing user"""
  updateUser(input: updateUserInput): updateUserPayload

  """Delete an existing user"""
  deleteUser(input: deleteUserInput): deleteUserPayload
  upload(refId: ID, ref: String, source: String, file: Upload!): UploadFile!
}

type Organization {
  id: ID!
  created_at: DateTime!
  updated_at: DateTime!
  name: String
  mail: String
  courierAddress: String
  phoneNumbers: String
  contracts(sort: String, limit: Int, start: Int, where: JSON): [Contract]
  reports(sort: String, limit: Int, start: Int, where: JSON): [Report]
  employees(sort: String, limit: Int, start: Int, where: JSON): [UsersPermissionsUser]
}

input OrganizationInput {
  name: String
  mail: String
  courierAddress: String
  contracts: [ID]
  reports: [ID]
  employees: [ID]
  phoneNumbers: String
}

type Query {
  contract(id: ID!): Contract
  contracts(sort: String, limit: Int, start: Int, where: JSON): [Contract]
  event(id: ID!): Events
  events(sort: String, limit: Int, start: Int, where: JSON): [Events]
  organization(id: ID!): Organization
  organizations(sort: String, limit: Int, start: Int, where: JSON): [Organization]
  report(id: ID!): Report
  reports(sort: String, limit: Int, start: Int, where: JSON): [Report]
  task(id: ID!): Tasks
  tasks(sort: String, limit: Int, start: Int, where: JSON): [Tasks]
  files(sort: String, limit: Int, start: Int, where: JSON): [UploadFile]
  role(id: ID!): UsersPermissionsRole

  """
  Retrieve all the existing roles. You can't apply filters on this query.
  """
  roles(sort: String, limit: Int, start: Int, where: JSON): [UsersPermissionsRole]
  user(id: ID!): UsersPermissionsUser
  users(sort: String, limit: Int, start: Int, where: JSON): [UsersPermissionsUser]
  me: UsersPermissionsMe
}

type Report {
  id: ID!
  created_at: DateTime!
  updated_at: DateTime!
  name: String
  dateCreate: DateTime
  editDate: DateTime
  departureDate: DateTime
  author: UsersPermissionsUser
  markDelete: Boolean
  organization: Organization
}

input ReportInput {
  name: String
  dateCreate: DateTime
  editDate: DateTime
  departureDate: DateTime
  author: ID
  markDelete: Boolean
  organization: ID
}

input RoleInput {
  name: String!
  description: String
  type: String
  permissions: [ID]
  users: [ID]
}

type Tasks {
  id: ID!
  created_at: DateTime!
  updated_at: DateTime!
  responsible: UsersPermissionsUser
  tasksTypes: ENUM_TASKS_TASKSTYPES
  name: String
  target: String
  result: String
  dateBegin: DateTime
  dateCreate: DateTime
  dateFinish: DateTime
  priority: ENUM_TASKS_PRIORITY
  expectedIncome: String
  content: String
  author: UsersPermissionsUser
  events(sort: String, limit: Int, start: Int, where: JSON): [Events]
  document(sort: String, limit: Int, start: Int, where: JSON): [UploadFile]
}

input TasksInput {
  responsible: ID
  tasksTypes: ENUM_TASKS_TASKSTYPES
  events: [ID]
  name: String
  target: String
  result: String
  dateBegin: DateTime
  dateCreate: DateTime
  dateFinish: DateTime
  priority: ENUM_TASKS_PRIORITY
  expectedIncome: String
  content: String
  document: [ID]
  author: ID
}

input updateContractInput {
  where: InputID
  data: editContractInput
}

type updateContractPayload {
  contract: Contract
}

input updateEventsInput {
  where: InputID
  data: editEventsInput
}

type updateEventsPayload {
  event: Events
}

input updateOrganizationInput {
  where: InputID
  data: editOrganizationInput
}

type updateOrganizationPayload {
  organization: Organization
}

input updateReportInput {
  where: InputID
  data: editReportInput
}

type updateReportPayload {
  report: Report
}

input updateRoleInput {
  where: InputID
  data: editRoleInput
}

type updateRolePayload {
  role: UsersPermissionsRole
}

input updateTasksInput {
  where: InputID
  data: editTasksInput
}

type updateTasksPayload {
  task: Tasks
}

input updateUserInput {
  where: InputID
  data: editUserInput
}

type updateUserPayload {
  user: UsersPermissionsUser
}

"""The `Upload` scalar type represents a file upload."""
scalar Upload

type UploadFile {
  id: ID!
  created_at: DateTime!
  updated_at: DateTime!
  name: String!
  hash: String!
  sha256: String
  ext: String
  mime: String!
  size: String!
  url: String!
  provider: String!
  public_id: String
  related(sort: String, limit: Int, start: Int, where: JSON): [Morph]
}

input UserInput {
  username: String!
  email: String!
  provider: String
  password: String
  resetPasswordToken: String
  confirmed: Boolean
  blocked: Boolean
  role: ID
  reports: [ID]
  organizations: ID
  phoneNumbers: String
  FIO: String
  authorEvents: ID
  responsibleEvent: ID
  authorTask: ID
  taskResponsible: ID
  memberEvents: ID
}

type UsersPermissionsMe {
  _id: ID!
  username: String!
  email: String!
  confirmed: Boolean
  blocked: Boolean
  role: UsersPermissionsMeRole
}

type UsersPermissionsMeRole {
  _id: ID!
  name: String!
  description: String
  type: String
}

type UsersPermissionsPermission {
  id: ID!
  type: String!
  controller: String!
  action: String!
  enabled: Boolean!
  policy: String
  role: UsersPermissionsRole
}

type UsersPermissionsRole {
  id: ID!
  name: String!
  description: String
  type: String
  permissions(sort: String, limit: Int, start: Int, where: JSON): [UsersPermissionsPermission]
  users(sort: String, limit: Int, start: Int, where: JSON): [UsersPermissionsUser]
}

type UsersPermissionsUser {
  id: ID!
  username: String!
  email: String!
  provider: String
  confirmed: Boolean
  blocked: Boolean
  role: UsersPermissionsRole
  organizations: Organization
  phoneNumbers: String
  FIO: String
  authorEvents: Events
  responsibleEvent: Events
  authorTask: Tasks
  taskResponsible: Tasks
  memberEvents: Events
  reports(sort: String, limit: Int, start: Int, where: JSON): [Report]
}
