'use strict';

/**
 * Contract.js controller
 *
 * @description: A set of functions called "actions" for managing `Contract`.
 */

module.exports = {

  /**
   * Retrieve contract records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.contract.search(ctx.query);
    } else {
      return strapi.services.contract.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a contract record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.contract.fetch(ctx.params);
  },

  /**
   * Count contract records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.contract.count(ctx.query);
  },

  /**
   * Create a/an contract record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.contract.add(ctx.request.body);
  },

  /**
   * Update a/an contract record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.contract.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an contract record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.contract.remove(ctx.params);
  }
};
