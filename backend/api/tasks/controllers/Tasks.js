'use strict';

/**
 * Tasks.js controller
 *
 * @description: A set of functions called "actions" for managing `Tasks`.
 */

module.exports = {

  /**
   * Retrieve tasks records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.tasks.search(ctx.query);
    } else {
      return strapi.services.tasks.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a tasks record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.tasks.fetch(ctx.params);
  },

  /**
   * Count tasks records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.tasks.count(ctx.query);
  },

  /**
   * Create a/an tasks record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.tasks.add(ctx.request.body);
  },

  /**
   * Update a/an tasks record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.tasks.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an tasks record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.tasks.remove(ctx.params);
  }
};
